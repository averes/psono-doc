---
title: Features for users
last_updated: Aug 20th, 2017
summary: "If you're not sure whether Psono will cover your needs, this list provides a semi-comprehensive overview of available features."
published: true
sidebar: user_sidebar
permalink: user_overview_supported_features.html
folder: user
---

Before you get into exploring Psono as a potential password manager, you may be wondering if it supports some basic
features needed to fulfill your requirements. The following table shows what is supported in Psono.

If you are looking more for features for admins, then you can find them here in our [Features for Admins](mydoc_overview_supported_features.html)

## Features for users

Features | Supported | Notes
--------|-----------|-----------
Client Side Encryption | Yes | All Data is encrypted in your client before it leaves your computer. The server never gets any secrets as plain text. |
Multilayer Transport Encryption | Yes | All traffic between the server and the client is protected by multiple layers of encryption with TLS 1.2 and Salsa20. |
Encryption at Rest | Yes | The data is encrypted by the server before its stored in the database. A full database dump will not even leak your email address. |
Server Pinning | Yes | The first time the client connects to a new server it will ask the user to verify the identity of the server. Every further connection is validated against this fingerprint to prevent spoofing |
Autofill | Yes | Login forms on websites and are automatically filled out. (This option can be disabled for every secret).  |
Basic Auth handling | Yes | Basic auth requests are handled automatically. (This option can be disabled for every secret).  |
Password Syncing | Yes | Passwords are synced securely across devices.  |
Password Sharing | Yes | Passwords can be shared between users and groups encrypted.  |
Multifactor Authentication | Yes | Psono supports the use of Yubikey, Duo and Google Authenticator (or other compatible apps like e.g. Authy). |
Open Source | Yes | Psono is completely open source, granting you the most flexible use and auditability. |
Multi Account Support | Yes | Support for multiple different logins for one website. |
Password Generator | Yes | A configurable random password generator. |
Secure Notes | Yes | Not only passwords but also notes can be saved, synced and shared with Psono. |
Bookmarks | Yes | Also bookmarks can be saved, synced and shared with Psono. |
Multi Browser Support | Yes | Native extensions exist for Firefox and Chrome, other browsers can use the webclient. |
Password Capture | Yes | Automatic capture of used passwords on the fly. |
Mobile Support | Yes | Mobile devices also have access to your passwords with our mobile friendly responsive design. |
Password Export | Yes | All stored secrets can be easily exported later. No "lock-in" if you later want to leave us. |
Password Import | Yes | Passwords of chrome browser and other password managers can be easily imported. |
Copy to Clipboard | Yes | Username and passwords can be easily copied to clipboard to allow a wider use, e.g. in standalone software. |
Groups | Yes | Groups of users with RBAC |
Access Control | Yes | Granular access control, allowing users to limit rights on shared content |
PGP Encryption | Yes | Encrypt and decrypt PGP messages. |
PGP Mailprovider integration | Yes | Allows to encrypt and decrypt gmail, outlook.com and yahoo mails directly from the respective website. |
Offline Mode | Yes | Access your passwords and other secrets offline. |
API Keys | Yes | Allows integration of passwords in build pipelines or startup scripts. |
Callbacks | Yes | Fire callbacks to specific urls whenever a secret changes in order to trigger automated actions e.g. restart XYZ |
Emergency Codes | Yes | Solving the digital legacy problem in case of emergencies or decease. |
Broad language support | Yes | Psono has been translated in alot of different languages. |
History of secrets | Yes | Old versions of secrets (e.g. passwords) are stored and are accessible in the history. |
File sharing | Yes | Share files either with on premise fileservers or external storage providers |
Callbacks | Yes | Automate your processes with configurable callbacks, that fire whenever the secret changes |
Easy Integratability | Yes | Integrate Psono with easily with simple clients (e.g. curl) into your infrastructure |
Link shares | Yes | Share secrets and files via link with others, even if they don't have an account |


{% include note.html content="If you are interested in a feature that is not listed, then let us know." %}


{% include links.html %}
