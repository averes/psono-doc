---
title: Setup Yubikey
tags: [2FA, userguide]
summary: "How to protect your account with a YubiKey"
sidebar: user_sidebar
permalink: user_2fa_yubikey.html
folder: user
---

## Preamble

We love Yubikey. They are one of the easiest way to protect your account against week nad even stolen passwords.
We strongly recommend setting up such a 2-Factor Authentication to protect your account. Once you call yourself the
proud owner of a Yubikey, follow the guide below. If you still don't have one, you can buy them online here: [yubico.com/store/](https://www.yubico.com/store/)
We advise you to setup always two Yubikeys, so you always have a spare one, that you can use in case your first Yubikey gets lost.

## Supported YubiKeys

They YubiKey needs to support Yubico OTP, which nearly all YubiKeys do, therefore we support:

* YubiKey 4
* YubiKey 4 nano
* YubiKey 4C
* YubiKey 4C nano
* YubiKey Neo
* YubiKey 5 NFC
* YubiKey 5 Nano
* YubiKey 5C
* YubiKey 5C Nano

{% include warning.html content="Not Supported are \"Fido U2F Security Keys\" and all other YubiKeys that are not listed here." %}

## Setup Guide

1)  Login into your account:

![Step 1 Login](images/user/2fa_yubikey/step1-login.jpg)

2)  Go to "Account":

![Step 2 go to account](images/user/2fa_yubikey/step2-go-to-account.jpg)

3)  Select the “Multifactor Authentication” tab:

![Step 3 go to multifactor authentication](images/user/2fa_yubikey/step3-go-to-multifactor-authentication.jpg)

4)  Click the "Configure" button next to Yubikey OTP:

![Step 4 click configure next to yubikey](images/user/2fa_yubikey/step4-click-configure-next-to-yubikey.jpg)

5)  Click the "New Yubikey" tab:

![Step 5 select new Yubikey](images/user/2fa_yubikey/step5-select-new-yubikey.jpg)

6)  Type in some descriptive text to identify your yubikeys later:

![Step 6 some descriptive title](images/user/2fa_yubikey/step6-some-descriptive-title.jpg)

7)  Put your cursor into the next box and "touch" your yubikey:

![Step 7 some descriptive title](images/user/2fa_yubikey/step7-put-your-cursor-into-the-yubikey-otp-box.jpg)

Well done, your first yubikey is now active. We strongly recommend setting up a second spare Yubikey, that you can use if you ever lose your first one.

{% include links.html %}
