---
title: Session less usage of the API key with remote decryption
tags: [API Key]
summary: "Overview of how to use the API key without sessions and remote decryption and filtering."
sidebar: user_sidebar
permalink: user_api_key_session_less_remote_decryption.html
folder: user
---

## Preamble

Psono allows to access secrets with simple tools like for example curl. In addition to the possibility to allow
the server to handle the decryption, psono allows you to filter the content. This makes it very simple to retrieve e.g.
passwords with curl without the need to handle the returned json object.

{% include note.html content="We highly discourage the usage of the session less usage with remote decryption." %}

## Example with cURL

```
curl -X POST \
  https://example.com/server/api-key-access/secret/ \
  -H 'content-type: multipart/form-data' \
  -F api_key_id=5e...af \
  -F secret_id=e4...d9 \
  -F api_key_secret_key=fd...31
```

Returns the decrypted and json encoded secret.

## Example with cURL and json_filter

You can filter that json on the server to only return parts of it (e.g. only the password) by providing a "json_filter".
which is a dot separated path to the desired element.

e.g. only "website_password_password" or "website_password_note.something"

The request would look like this:

```
curl -X POST \
  https://example.com/server/api-key-access/secret/ \
  -H 'content-type: multipart/form-data' \
  -F api_key_id=5e...af \
  -F secret_id=e4...d9 \
  -F api_key_secret_key=fd...31 \
  -F json_filter=website_password_password.something
```


{% include links.html %}
