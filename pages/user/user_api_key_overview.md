---
title: API Key Overview
tags: [API Key]
summary: "Overview of how to use the API Key."
sidebar: user_sidebar
permalink: user_api_key_overview.html
folder: user
---

## Preamble

There are multiple ways to use the API key. Each of them has its own benefits and drawbacks. Use the list below to
choose the best option for you.

## Overview

In general there are three ways to use the API Key:

a) Session Based

b) Sessionless With Local Decryption

c) Sessionless With Remote Decryption


|   | Session Based | Sessionless With Local Decryption | Sessionless With Remote Decryption |
| - | --------- | ----------------- | ----------------- |
| Usecase | Build pipelines / Console client as alternative to web based clients. | Implementation in code to dynamically load specific secrets | Buildpipelines as proof of concept when the console client for some reason is no option |
| Permission | Read & Write of Datastores, Secrets, ... | Read of specific Secrets | Read of specific Secrets |
| Implementation Effort | medium (with the official client) | high | low |
| Simple client support (e.g. curl) | No | No | Yes |
| Server gets temporary access to decrypted data | No | No | Yes |
| Necessity to decrypt data locally | Yes | Yes | No |
| Data Encryption | Yes | Yes | No |
| Additional Transport Encryption | Yes | No | No |


{% include note.html content="We highly discourage the usage of the sessionless usage with remote decryption." %}



{% include links.html %}
