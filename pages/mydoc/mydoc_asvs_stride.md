---
title: STRIDE
tags: [asvs, owasp]
summary: "STRIDE thread categorization."
sidebar: mydoc_sidebar
permalink: mydoc_asvs_stride.html
folder: mydoc
---

# Overview

* Spoofing: Identity "Identity spoofing" is a key risk for applications that have many users but provide a single execution context at the application and database level. In particular, users should not be able to become any other user or assume the attributes of another user.

* Tampering: with Data Users can potentially change data delivered to them, return it, and thereby potentially manipulate client-side validation, GET and POST results, cookies, HTTP headers, and so forth. The application should not send data to the user, such as interest rates or periods, which are obtainable only from within the application itself. The application should also carefully check data received from the user and validate that it is sane and applicable before storing or using it.

* Repudiation:  Users may dispute transactions if there is insufficient auditing or recordkeeping of their activity. For example, if a user says, “But I didn’t transfer any money to this external account!”, and you cannot track his/her activities through the application, then it is extremely likely that the transaction will have to be written off as a loss.
Therefore, consider if the application requires non-repudiation controls, such as web access logs, audit trails at each tier, or the same user context from top to bottom. Preferably, the application should run with the user’s privileges, not more, but this may not be possible with many off-the-shelf application frameworks.

* Information Disclosure: Users are rightfully wary of submitting private details to a system. If it is possible for an attacker to publicly reveal user data at large, whether anonymously or as an authorized user, there will be an immediate loss of confidence and a substantial period of reputation loss. Therefore, applications must include strong controls to prevent user ID tampering and abuse, particularly if they use a single context to run the entire application.
Also, consider if the user’s web browser may leak information. Some web browsers may ignore the no caching directives in HTTP headers or handle them incorrectly. In a corresponding fashion, every secure application has a responsibility to minimize the amount of information stored by the web browser, just in case it leaks or leaves information behind, which can be used by an attacker to learn details about the application, the user, or to potentially become that user.
Finally, in implementing persistent values, keep in mind that the use of hidden fields is insecure by nature. Such storage should not be relied on to secure sensitive information or to provide adequate personal privacy safeguards.

* Denial of Service:  Application designers should be aware that their applications may be subject to a denial of service attack. Therefore, the use of expensive resources such as large files, complex calculations, heavy-duty searches, or long queries should be reserved for authenticated and authorized users, and not available to anonymous users.
For applications that do not have this luxury, every facet of the application should be engineered to perform as little work as possible, to use fast and few database queries, to avoid exposing large files or unique links per user, in order to prevent simple denial of service attacks.

* Elevation of Privilege:  If an application provides distinct user and administrative roles, then it is vital to ensure that the user cannot elevate his/her role to a higher privilege one. In particular, simply not displaying privileged role links is insufficient. Instead, all actions should be gated through an authorization matrix, to ensure that only the permitted roles can access privileged functionality.


# Analysis

| Nr. | Thread | Threat |
| --- | ------ | ------ |
| 001 | Viewing or tampering with the data from the web client to the reverse proxy (MITM) | **S** (e.g. impersonate the user)<br>**T** (e.g. manipulate a new password)<br>**I** (e.g. gain access to passwords)<br>**D**(e.g. don't serve the webclient)<br>**E**(e.g. if the attacked user is a superuser) |
| 002 | Viewing or tampering with the data from the admin web client to the reverse proxy (MITM) |  **S** (e.g. impersonate the user)<br>**T** (e.g. manipulate health check response)<br>**I** (e.g. gain access to email addresses)<br>**D**(e.g. delete other users sessions)<br>**E**(e.g. attacked user is a superuser) |
| 004 | Viewing or tampering with the data from the reverse proxy to the server (MITM) | **S** (e.g. impersonate the user)<br>**T** (e.g. manipulate a new password)<br>**I** (e.g. gain access to passwords)<br>**D**(e.g. don't serve the webclient)<br>**E**(e.g. if the attacked user is a superuser) |
| 005 | Viewing or tampering with the data from the server to the database (MITM) | **T** (e.g. manipulate links)<br>**I** (e.g. gain access to stored passwords)<br>**D** (e.g. delete users)<br>**E** (e.g. promote own user to superuser) |
| 006 | Viewing or tampering with the data from the server to the email server (MITM) | **I** (e.g. e-mail addresses)<br>**D** (e.g. block registration)<br>**T** (e.g. impersonate email server and send links to phishing website) |
| 007 | Viewing or tampering with the data in the database  | **S** (e.g. manipulate passwords, API tokens)<br>**T** (e.g. change urls to websites)<br>**I** (e.g. e-mail addresses)<br>**D** (e.g. corrupt data, making it impossible to login) |
| 008 | Viewing or tampering with the web client application  | **T** (e.g. send requests to other server)<br>**I** (e.g. send password to other backend)<br>**D** (e.g. deface the website)<br>**E** (e.g. catched passwords can be used to login) |
| 009 | Viewing or tampering with the server application  | **S** (e.g. forward API tokens to third parties)<br>**T** (e.g. manipulate stored data)<br>**R** (e.g. prevent logging)<br>**I** (e.g. access user data)<br>**D** (e.g. block requests)<br>**E** (e.g. catched passwords can be used to login) |
| 010 | A malicious user deletes logs  | **R** (e.g. deletes audit logs) |
| 012 | DDoS against the database  | **D** (noone is able to login) |
| 013 | DDoS against the backend server  | **D** (noone is able to login) |
| 014 | DDoS against the relay proxy  | **D** (noone is able to login) |
| 015 | DDoS against the web client  | **D** (noone is able to login) |
| 016 | DDoS against the admin web client  | **D** (admins are not able to login) |
| 017 | DDoS against the email server  | **D** (noone can register) |
| 018 | DDoS against the time server  | **R** (e.g. audit logs become "unreadable")<br>**D** (some login methods wont work with a wrong time) |
| 019 | Bruteforce attack against the login form  | **S** (e.g. impersonate the user)<br>**T** (e.g. change user data)<br>**I** (e.g. disclose private information)<br>**E** (e.g. if the target is an admin user) |
| 020 | Bruteforce attack against the password recovery form  | **S** (e.g. impersonate the user)<br>**T** (e.g. change user data)<br>**I** (e.g. disclose private information)<br>**E** (e.g. if the target is an admin user) |
| 021 | SQL injection  | **S** (e.g. impersonate the user)<br>**T** (e.g. change user data)<br>**I** (e.g. disclose private information)<br>**E** (e.g. bypass validation checks for admin roles) |
| 022 | LDAP injection  | **S** (e.g. impersonate the user)<br>**T** (e.g. change user data)<br>**I** (e.g. disclose private information)<br>**E** (e.g. bypass validation checks for admin roles) |
| 023 | Cross-site Scripting (XSS)  | **S** (e.g. impersonate the user)<br>**T** (e.g. change user data)<br>**I** (e.g. disclose private information)<br>**E** (e.g. trick client to send data to the wrong url) |
| 024 | Software Vulnerability | **S** (e.g. weak random number generator)<br>**T** (e.g. gain access and tamper with any data)<br>**I** (e.g. causing crashes that reveal sensitive data)<br>**D** (e.g. causing crashes)<br>**E** (e.g. bypass security controls) |
| 025 | Security Missconfiguration | **S** (e.g. weak configuration of authentication methods)<br>**T** (e.g. gain access and tamper with any data)<br>**I** (e.g. weak crypto)<br>**D** (e.g. filepointer too low)<br>**E** (e.g. bypass security controls) |



## Source

[https://www.owasp.org/index.php/Threat_Risk_Modeling#STRIDE](https://www.owasp.org/index.php/Threat_Risk_Modeling#STRIDE)