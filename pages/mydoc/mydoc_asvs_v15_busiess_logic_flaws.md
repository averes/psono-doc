---
title: V15 Business logic flaws
tags: [asvs, owasp]
summary: "Business logic verification requirements"
sidebar: mydoc_sidebar
permalink: mydoc_asvs_v15_busiess_logic_flaws.html
folder: mydoc
---

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     | 
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------| 
| 15.1                        | Verify the application will only process business logic flows in sequential step order, with all steps being processed in realistic human time, and not process out of order, skipped steps, process steps from another user, or too quickly submitted transactions.                                                                                           |                         | x          | x           | 2.0       | 
| 15.2                        | Verify the application has business limits and correctly enforces on a per user basis, with configurable alerting and automated reactions to automated or unusual attack.                                                                                                                                                                                      |                         | x          | x           | 2.0       | 

