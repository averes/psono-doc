---
title: V19 Configuration
tags: [asvs, owasp]
summary: "Configuration"
sidebar: mydoc_sidebar
permalink: mydoc_asvs_v19_configuration.html
folder: mydoc
---

## ASVS Verification Requirement

| ID          | Detailed Verification Requirement                                                                                                                                                                                                                              | Level 1 | Level 2    | Level 3     | Since     | 
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------| 
| 19.1        | All components should be up to date with proper security configuration(s) and version(s). This should include removal of unneeded configurations and folders such as sample applications, platform documentation, and default or example users.                | x       | x          | x           | 3.0       | 
| 19.2        | Communications between components, such as between the application server and the database server, should be encrypted, particularly when the components are in different containers or on different systems.                                                  |         | x          | x           | 3.0       | 
| 19.3        | Communications between components, such as between the application server and the database server should be authenticated using an account with the least necessary privileges.                                                                                |         | x          | x           | 3.0       | 
| 19.4        | Verify application deployments are adequately sandboxed, containerized or isolated to delay and deter attackers from attacking other applications.                                                                                                             |         | x          | x           | 3.0       | 
| 19.5        | Verify that the application build and deployment processes are performed in a secure fashion.                                                                                                                                                                  |         | x          | x           | 3.0       | 
| 19.6        | Verify that authorised administrators have the capability to verify the integrity of all security-relevant configurations to ensure that they have not been tampered with.                                                                                     |         |            | x           | 3.0       | 
| 19.7        | Verify that all application components are signed.                                                                                                                                                                                                             |         |            | x           | 3.0       | 
| 19.8        | Verify that third party components come from trusted repositories.                                                                                                                                                                                             |         |            | x           | 3.0       | 
| 19.9        | Ensure that build processes for system level languages have all security flags enabled, such as ASLR, DEP, and security checks.                                                                                                                                |         |            | x           | 3.0       | 
| 19.10       | Verify that all application assets are hosted by the application, such as JavaScript libraries, CSS stylesheets and web fonts are hosted by the application rather than rely on a CDN or external provider.                                                    |         |            | x           | 3.0.1     | 



