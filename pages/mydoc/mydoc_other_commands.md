---
title: Commands
tags: [server]
summary: "An overview of the commands offered by the psono server"
sidebar: mydoc_sidebar
permalink: mydoc_other_commands.html
folder: mydoc
---

## Commands

The psono server offers some commands:


### cleartoken

Clears all expired tokens

	python3 ./psono/manage.py cleartoken

(usually executed in  a cronjob)


### createuser

Creates a user with a given username, password and email

	python3 ./psono/manage.py createuser username@example.com myPassword email@something.com

(usually executed by developers when they want to create a new user for testing)


### deleteuser

Deletes a user with a given username

	python3 ./psono/manage.py deleteuser username@example.com

(usually executed by developers when they want to cleanup after testing, or want to try again with a fresh user)


### disableuser

Disables a user. The user won't be able to login anymore.

	python3 ./psono/manage.py disableuser username@example.com

### enableuser

Enables a user. The user will be able to login again.

	python3 ./psono/manage.py enableuser username@example.com

### fsclustercreate

Creates a cluster. Takes a title of the cluster as argument.

	python3 ./psono/manage.py fsclustercreate "Default Cluster"

### fsclusterdelete

Deletes a cluster. Takes the id of a cluster as argument

	python3 ./psono/manage.py fsclusterdelete 1713b68b-df64-41c5-822c-6eb8c877037c

### fsclusterlist

Lists all configured clusters

	python3 ./psono/manage.py fsclusterlist

### fsclustershowconfig

Creates the configuration of a potential cluster member for a cluster. Takes the id of a cluster as argument.

	python3 ./psono/manage.py fsclustershowconfig 1713b68b-df64-41c5-822c-6eb8c877037c

### fsshardcreate

Creates a shard with a title and a description as argument

	python3 ./psono/manage.py fsshardcreate "Some Title" "Some description"

(The title will be later visible to the user if multiple shards are available, so the user can pick one)

### fssharddelete

Deletes a shard. Takes the id of the shard that should be deleted as argument.

	python3 ./psono/manage.py fssharddelete a8a1176c-bad8-4b84-b45e-1084b3a48e7d

### fsshardlink

Links a shard and a cluster together, allowing fileservers that belong to a cluster to announce their availability to serve
a shard. Takes the cluster id and the shard id as arguments and in addition the permissions (e.g. rw for read and write)

	python3 ./psono/manage.py fsshardlink 1713b68b-df64-41c5-822c-6eb8c877037c a8a1176c-bad8-4b84-b45e-1084b3a48e7d rw

### fsshardlist

Lists all configured shards.

	python3 ./psono/manage.py fsshardlist

### fsshardunlink

Destroys the link between a shard and a cluster, preventing fileservers of this clusters to announce this shard.
Takes the ID of the link as argument.

	python3 ./psono/manage.py fsshardunlink c8916ab1-4a22-4aa6-9549-c0b15f688e7c

### promoteuser

Promotes a user and grants superuser privilege (or other roles)

	python3 ./psono/manage.py promoteuser username@example.com superuser

### demoteuser

Demotes a user and revokes superuser privilege (or other roles)

	python3 ./psono/manage.py demoteuser username@example.com superuser

### generateserverkeys

Generates some new server keys for the settings.yaml

	python3 ./psono/manage.py generateserverkeys

(usually executed only once during the installation phase)

### presetup

If the database user has the necessary permission this command will install the necessary postgres extensions ltree and pgcrypto.

	python3 ./psono/manage.py presetup

(usually executed only once during the installation phase)


### sendtestmail

Sends a test email to the given email address.

	python3 ./psono/manage.py sendtestmail something@something.com

(usually executed only during isntallation, to test if the email configuration is correct)


### testldap

Tests your LDAP connection (EE Version only)

	python3 ./psono/manage.py testldap username@something.com thePassWord

(usually executed only during isntallation, to test if the LDAP configuration is correct)


### testsaml

Tests your SAML connection (EE Version only)

	python3 ./psono/manage.py testsaml

(usually executed only during isntallation, to test if the SAML configuration is correct)


