---
title: Development browser extension
tags: [development, client]
summary: "Everything around the development of the browser extension"
sidebar: mydoc_sidebar
permalink: mydoc_development_browser_extension.html
folder: mydoc
---

The browser extensions are nothing but some special packaged web clients, with some additional sugar for panels and so on.

## Preamble

This whole guide is based on Ubuntu 18.04 LTS. Ubuntu 12.04+ LTS and Debian based systems should be similar if not even
identical.

We assume that you already have somewhere a Psono client development environment up. If not follow the [guide to develop clients](mydoc_development_clients.html).

## Development

There are alot of different ways to develop browser extensions. The most simple one is to create the unpackaged files
and then load them in the browser:

### Chrome

1.  Build

	To build the browser extension execute the following command:

		gulp
		
2.  Follow this guide and load the extension from `psono-client/dist/chrome`:

	[https://developer.chrome.com/extensions/getstarted#unpacked](https://developer.chrome.com/extensions/getstarted#unpacked)

	In short:
	
	1.  open Chrome
    2.  enter "chrome://extensions" in the URL bar
    3.  Ensure that the Developer mode checkbox in the top right-hand corner is checked.
    4.  click "Load unpacked extension…"
    5.  Navigate to `psono-client/dist/chrome`, and select it.

### Firefox

1.  Build

	To build the browser extension execute the following command:

		gulp

2.  Follow this guide and load the extension from `psono-client/dist/firefox`:
    
    [https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Temporary_Installation_in_Firefox](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Temporary_Installation_in_Firefox)

	In short:
	
	1.  open Firefox
    2.  enter "about:debugging" in the URL bar
    3.  click "Load Temporary Add-on"
    4.  open `psono-client/dist/firefox` and select any file inside.


## Release

### Chrome

The instruction will describe how to build / package and deploy the Chrome browser extension

1.  Build

	To build the browser extension execute the following command:

		gulp


2.  Packaging

	First, we create the dist folder if it does not yet exist:

		mkdir -p psono-client/dist/chrome

	Second, we go into the build folder for the Chrome extension:

		cd psono-client/build/chrome/

	Afterwards we just zip the content:

		zip -r ../../dist/chrome/psono.chrome.PW.zip *
		
3.  Deployment

	To deploy everything now to the Chrome Web Store execute the following:

		gulp --webstore_client_id=$webstore_client_id \
			--webstore_client_secret=$webstore_client_secret \
	        --webstore_refresh_token=$webstore_refresh_token \
	        --webstore_app_id=$webstore_app_id \
	        chrome-deploy
	        
	This will look for a `dist/chrome/psono.chrome.PW.zip` and deploy it. Replace the following variables:

		$webstore_client_id         # e.g.: 12345678-XXXXXXXXX...XXXX.apps.googleusercontent.com
		$webstore_client_secret     # e.g.: ABCDEFg-AaaaAAaaAKaasd9
		$webstore_refresh_token     # X/XXXXXxxxxXXXXX_XXxxxXxXXXxxx
		$webstore_app_id            # e.g.: eljmjmgjkbmpmfljlmklcfineebidmlo
	
More infos can be found here:

- [https://www.npmjs.com/package/webstore-upload](https://www.npmjs.com/package/webstore-upload)

- [https://developer.chrome.com/webstore/using_webstore_api](https://developer.chrome.com/webstore/using_webstore_api)
	
		
### Firefox

The instruction will describe how to build / package and deploy the Firefox browser extension

1.  Build

	To build the browser extension execute the following command:

		gulp

2.  Packaging

	First, we create the dist folder if it does not yet exist:

		mkdir -p psono-client/dist/firefox

	Second, we go into the build folder for the Firefox extension:

		cd psono-client/build/firefox/

	Afterwards we just zip the content:

		zip -r ../../dist/firefox/psono.firefox.PW.zip *
		
3.  Deployment

	To deploy everything now to Firefox Add-ons execute the following:

		gulp --mozilla_jwt_issuer=$mozilla_jwt_issuer \
			--mozilla_jwt_secret=$mozilla_jwt_secret \
			--mozilla_addon_id=$mozilla_addon_id \
			--mozilla_version=$(echo $CI_COMMIT_TAG | awk  '{ string=substr($0, 2, 100); print string; }' ) \
			firefox-deploy

	This will look for a `dist/chrome/psono.firefox.PW.zip` and deploy it. Replace the following variables:

		$mozilla_jwt_issuer         # e.g.: user:12345689:123
		$mozilla_jwt_secret         # e.g.: asdasdasd45654334456asdasdasd6345645456asdasdsad456456456
		$mozilla_addon_id           # e.g.: {4cea82d7-5815-47eb-a7fe-2a5fd4974126}

To make this work the current repo needs to be tagged with something like "v1.0.0", and be aware that duplicate versions
will be rejected.

More infos can be found here: 

- [https://addons-server.readthedocs.io/en/latest/topics/api/signing.html](https://addons-server.readthedocs.io/en/latest/topics/api/signing.html)


{% include links.html %}

