---
title: User Documentation for Psono
sidebar: user_sidebar
permalink: index.html
summary: These brief instructions will help you get started quickly with the psono password manager. The other topics in this help provide additional information and detail about working with other aspects.
---

If you are an admin or developer and are just looking for the appropriate documentation, then please follow [this link here](mydoc_index.html) or use the "Documentation Type" link at the top to switch.

## Overview

The Psono password manager consists of two components, the client and the server:

1.  Client

    The client comes is usually the thing that you "start" and accesses your data that is stored on the server.
    Its your main working utility and it comes (currently) in three flavors.

    a) As a web site, that your administrator will setup for you. A demo of the web site can be found on [psono.pw](https://www.psono.pw)

    b) As a [Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/psono-pw-password-manager/)

    c) As a [Chrome extension](https://chrome.google.com/webstore/detail/psonopw-password-manager/eljmjmgjkbmpmfljlmklcfineebidmlo)


2.  Server

    The server is the "core" of the system, doing all the logic, and stores your data. Before you register / login you can point your
    client to use a different server, but thats the only "interaction" that you have.


![Overview of a typical setup](images/user_setup.png)

{% include note.html content="All Secrets (Passwords, Notes, Bookmarks, ...) are encrypted on your PC in your browser, before they are stored on the server." %}

{% include links.html %}
